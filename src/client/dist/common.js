"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var tf = require("@tensorflow/tfjs");
var tfjs_1 = require("@tensorflow/tfjs");
function serializeVar(variable) {
    return __awaiter(this, void 0, void 0, function () {
        var data, copy;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, variable.data()];
                case 1:
                    data = _a.sent();
                    copy = data.buffer.slice(data.byteOffset, data.byteOffset + data.byteLength);
                    return [2, { dtype: variable.dtype, shape: variable.shape.slice(), data: copy }];
            }
        });
    });
}
exports.serializeVar = serializeVar;
function serializeVars(vars) {
    return __awaiter(this, void 0, void 0, function () {
        var varsP;
        return __generator(this, function (_a) {
            varsP = [];
            vars.forEach(function (value, key) {
                var lv = value;
                if (lv.write != null) {
                    varsP.push(serializeVar(lv.read()));
                }
                else {
                    varsP.push(serializeVar(lv));
                }
            });
            return [2, Promise.all(varsP)];
        });
    });
}
exports.serializeVars = serializeVars;
function deserializeVar(serialized) {
    var dtype = serialized.dtype, shape = serialized.shape, dataBuffer = serialized.data;
    var data;
    if (dataBuffer instanceof ArrayBuffer) {
        data = dataBuffer;
    }
    else if (dataBuffer instanceof Buffer) {
        var dataAsBuffer = dataBuffer;
        data = dataAsBuffer.buffer.slice(dataAsBuffer.byteOffset, dataAsBuffer.byteOffset + dataAsBuffer.byteLength);
    }
    var numel = shape.reduce(function (x, y) { return x * y; }, 1);
    var ctor = dtypeToTypedArrayCtor[dtype];
    var array = new ctor(data, 0, numel);
    return tf.tensor(array, shape, dtype);
}
exports.deserializeVar = deserializeVar;
function tensorToJson(t) {
    return __awaiter(this, void 0, void 0, function () {
        var data, lv;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    lv = t;
                    if (!(lv.write != null)) return [3, 2];
                    return [4, lv.read().data()];
                case 1:
                    data = _a.sent();
                    return [3, 4];
                case 2: return [4, t.data()];
                case 3:
                    data = _a.sent();
                    _a.label = 4;
                case 4: return [2, { 'values': Array.from(data), 'shape': t.shape, 'dtype': t.dtype }];
            }
        });
    });
}
exports.tensorToJson = tensorToJson;
function jsonToTensor(j) {
    return tf.tensor(j.values, j.shape, j.dtype || 'float32');
}
exports.jsonToTensor = jsonToTensor;
function serializedToJson(s) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2, tensorToJson(deserializeVar(s))];
        });
    });
}
exports.serializedToJson = serializedToJson;
function jsonToSerialized(j) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2, serializeVar(jsonToTensor(j))];
        });
    });
}
exports.jsonToSerialized = jsonToSerialized;
var dtypeToTypedArrayCtor = {
    'float32': Float32Array,
    'int32': Int32Array,
    'bool': Uint8Array
};
var FederatedTfModel = (function () {
    function FederatedTfModel(model, config) {
        this.model = model;
        this.config = config || { epochs: 10, batchSize: 32 };
    }
    FederatedTfModel.prototype.fit = function (x, y) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4, this.model.fit(x, y, this.config)];
                    case 1:
                        _a.sent();
                        return [2];
                }
            });
        });
    };
    FederatedTfModel.prototype.getVars = function () {
        return this.model.trainableWeights.map(function (v) { return v.read(); });
    };
    FederatedTfModel.prototype.setVars = function (vals) {
        for (var i = 0; i < vals.length; i++) {
            this.model.trainableWeights[i].write(vals[i]);
        }
    };
    return FederatedTfModel;
}());
exports.FederatedTfModel = FederatedTfModel;
var FederatedDynamicModel = (function () {
    function FederatedDynamicModel(vars, loss, optimizer) {
        this.vars = vars;
        this.loss = loss;
        this.optimizer = optimizer;
    }
    FederatedDynamicModel.prototype.fit = function (x, y) {
        return __awaiter(this, void 0, void 0, function () {
            var lossVal;
            var _this = this;
            return __generator(this, function (_a) {
                lossVal = this.optimizer.minimize(function () { return _this.loss(x, y); });
                if (lossVal) {
                    lossVal.dispose();
                }
                return [2];
            });
        });
    };
    FederatedDynamicModel.prototype.getVars = function () {
        return this.vars.slice();
    };
    FederatedDynamicModel.prototype.setVars = function (vals) {
        for (var i = 0; i < vals.length; i++) {
            this.vars[i].assign(vals[i]);
        }
    };
    return FederatedDynamicModel;
}());
exports.FederatedDynamicModel = FederatedDynamicModel;
function federated(model) {
    if (model instanceof tfjs_1.Model) {
        return new FederatedTfModel(model);
    }
    else {
        return model;
    }
}
exports.federated = federated;
var Events;
(function (Events) {
    Events["Download"] = "downloadVars";
    Events["Upload"] = "uploadVars";
    Events["Data"] = "uploadData";
})(Events = exports.Events || (exports.Events = {}));
var LOGGING_ENABLED = (process.env != null && !!process.env.VERBOSE) || false;
function verbose(enabled) {
    LOGGING_ENABLED = enabled;
}
exports.verbose = verbose;
function log() {
    var args = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        args[_i] = arguments[_i];
    }
    if (LOGGING_ENABLED) {
        console.error.apply(console, args);
    }
}
exports.log = log;
//# sourceMappingURL=common.js.map