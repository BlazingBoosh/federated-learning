import * as tf from '@tensorflow/tfjs';
import { DownloadMsg, FederatedModel, SerializedVariable, HyperParamsMsg } from './common';
import { Model } from '@tensorflow/tfjs';
export declare type DownloadCallback = (msg: DownloadMsg) => void;
export declare class ClientAPI {
    private msg;
    private model;
    private socket;
    private downloadCallbacks;
    constructor(model: FederatedModel | Model);
    modelVersion(): string;
    onDownload(callback: DownloadCallback): void;
    connect(serverURL: string): Promise<void>;
    dispose(): void;
    uploadData(x: tf.Tensor, y: tf.Tensor): Promise<{}>;
    federatedUpdate(xs: tf.Tensor, ys: tf.Tensor): Promise<void>;
    hyperparams(): HyperParamsMsg;
    private uploadVars;
    protected setVars(newVars: SerializedVariable[]): void;
    private connectTo;
}
