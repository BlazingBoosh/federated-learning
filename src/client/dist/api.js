"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var tf = require("@tensorflow/tfjs");
var socketProxy = require("socket.io-client");
var socketio = socketProxy.default || socketProxy;
var common_1 = require("./common");
var CONNECTION_TIMEOUT = 10 * 1000;
var UPLOAD_TIMEOUT = 5 * 1000;
var ClientAPI = (function () {
    function ClientAPI(model) {
        this.model = common_1.federated(model);
        this.downloadCallbacks = [function (msg) {
                common_1.log('download', 'modelVersion:', msg.modelVersion);
                common_1.log('hyperparams', 'hyperparams:', msg.hyperparams);
            }];
    }
    ClientAPI.prototype.modelVersion = function () {
        return this.msg.modelVersion;
    };
    ClientAPI.prototype.onDownload = function (callback) {
        this.downloadCallbacks.push(callback);
    };
    ClientAPI.prototype.connect = function (serverURL) {
        return __awaiter(this, void 0, void 0, function () {
            var msg;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4, this.connectTo(serverURL)];
                    case 1:
                        msg = _a.sent();
                        this.msg = msg;
                        this.setVars(msg.vars);
                        this.downloadCallbacks.forEach(function (cb) { return cb(msg); });
                        this.socket.on(common_1.Events.Download, function (msg) {
                            _this.msg = msg;
                            _this.setVars(msg.vars);
                            _this.downloadCallbacks.forEach(function (cb) { return cb(msg); });
                        });
                        return [2];
                }
            });
        });
    };
    ClientAPI.prototype.dispose = function () {
        this.socket.disconnect();
        common_1.log('disconnected');
    };
    ClientAPI.prototype.uploadData = function (x, y) {
        return __awaiter(this, void 0, void 0, function () {
            var msg, _a, prom;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = {};
                        return [4, common_1.serializeVar(x)];
                    case 1:
                        _a.x = _b.sent();
                        return [4, common_1.serializeVar(y)];
                    case 2:
                        msg = (_a.y = _b.sent(), _a);
                        prom = new Promise(function (resolve, reject) {
                            var rejectTimer = setTimeout(function () { return reject("uploadData timed out"); }, UPLOAD_TIMEOUT);
                            _this.socket.emit(common_1.Events.Data, msg, function () {
                                clearTimeout(rejectTimer);
                                resolve();
                                common_1.log('uploadData');
                            });
                        });
                        return [2, prom];
                }
            });
        });
    };
    ClientAPI.prototype.federatedUpdate = function (xs, ys) {
        return __awaiter(this, void 0, void 0, function () {
            var modelVersion, newVars;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        modelVersion = this.msg.modelVersion;
                        return [4, this.model.fit(xs, ys)];
                    case 1:
                        _a.sent();
                        return [4, common_1.serializeVars(this.model.getVars())];
                    case 2:
                        newVars = _a.sent();
                        this.setVars(this.msg.vars);
                        return [4, this.uploadVars({ modelVersion: modelVersion, numExamples: xs.shape[0], vars: newVars })];
                    case 3:
                        _a.sent();
                        return [2];
                }
            });
        });
    };
    ClientAPI.prototype.hyperparams = function () {
        return this.msg.hyperparams;
    };
    ClientAPI.prototype.uploadVars = function (msg) {
        return __awaiter(this, void 0, void 0, function () {
            var prom;
            var _this = this;
            return __generator(this, function (_a) {
                prom = new Promise(function (resolve, reject) {
                    var rejectTimer = setTimeout(function () { return reject("uploadVars timed out"); }, UPLOAD_TIMEOUT);
                    _this.socket.emit(common_1.Events.Upload, msg, function () {
                        clearTimeout(rejectTimer);
                        resolve();
                    });
                });
                return [2, prom];
            });
        });
    };
    ClientAPI.prototype.setVars = function (newVars) {
        var _this = this;
        tf.tidy(function () {
            _this.model.setVars(newVars.map(function (v) { return common_1.deserializeVar(v); }));
        });
    };
    ClientAPI.prototype.connectTo = function (serverURL) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.socket = socketio(serverURL);
                return [2, fromEvent(this.socket, common_1.Events.Download, CONNECTION_TIMEOUT)];
            });
        });
    };
    return ClientAPI;
}());
exports.ClientAPI = ClientAPI;
function fromEvent(emitter, eventName, timeout) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2, new Promise(function (resolve, reject) {
                    var rejectTimer = setTimeout(function () { return reject(eventName + " event timed out"); }, timeout);
                    var listener = function (evtArgs) {
                        emitter.removeListener(eventName, listener);
                        clearTimeout(rejectTimer);
                        resolve(evtArgs);
                    };
                    emitter.on(eventName, listener);
                })];
        });
    });
}
//# sourceMappingURL=api.js.map