import * as tf from '@tensorflow/tfjs';
import { Model, ModelFitConfig, Optimizer, Scalar, Tensor, Variable } from '@tensorflow/tfjs';
import { LayerVariable } from '@tensorflow/tfjs-layers/dist/variables';
export declare type SerializedVariable = {
    dtype: tf.DataType;
    shape: number[];
    data: ArrayBuffer;
};
export declare function serializeVar(variable: tf.Tensor): Promise<SerializedVariable>;
export declare function serializeVars(vars: Array<Variable | LayerVariable | Tensor>): Promise<SerializedVariable[]>;
export declare function deserializeVar(serialized: SerializedVariable): tf.Tensor;
export declare type TensorJson = {
    values: number[];
    shape: number[];
    dtype?: tf.DataType;
};
export declare type ModelJson = {
    vars: TensorJson[];
};
export declare type UpdateJson = {
    numExamples: number;
    vars: TensorJson[];
    modelVersion?: string;
    clientId?: string;
};
export declare type DataJson = {
    x: TensorJson;
    y: TensorJson;
    clientId?: string;
};
export declare function tensorToJson(t: tf.Tensor): Promise<TensorJson>;
export declare function jsonToTensor(j: TensorJson): tf.Tensor;
export declare function serializedToJson(s: SerializedVariable): Promise<TensorJson>;
export declare function jsonToSerialized(j: TensorJson): Promise<SerializedVariable>;
export declare type VarList = Array<Tensor | Variable | LayerVariable>;
export interface FederatedModel {
    fit(x: Tensor, y: Tensor): Promise<void>;
    getVars(): VarList;
    setVars(vals: Tensor[]): void;
}
export declare class FederatedTfModel implements FederatedModel {
    private model;
    private config;
    constructor(model: Model, config?: ModelFitConfig);
    fit(x: Tensor, y: Tensor): Promise<void>;
    getVars(): VarList;
    setVars(vals: Tensor[]): void;
}
export declare class FederatedDynamicModel implements FederatedModel {
    vars: Variable[];
    loss: (xs: Tensor, ys: Tensor) => Scalar;
    optimizer: Optimizer;
    constructor(vars: Variable[], loss: (xs: Tensor, ys: Tensor) => Scalar, optimizer: Optimizer);
    fit(x: Tensor, y: Tensor): Promise<void>;
    getVars(): Variable[];
    setVars(vals: Tensor[]): void;
}
export declare function federated(model: FederatedDynamicModel | FederatedModel | Model): FederatedModel;
export declare enum Events {
    Download = "downloadVars",
    Upload = "uploadVars",
    Data = "uploadData"
}
export declare type UploadMsg = {
    modelVersion: string;
    vars: SerializedVariable[];
    numExamples: number;
};
export declare type DataMsg = {
    x: SerializedVariable;
    y: SerializedVariable;
};
export declare type DownloadMsg = {
    modelVersion: string;
    vars: SerializedVariable[];
    hyperparams: HyperParamsMsg;
};
export declare type HyperParamsMsg = object;
export declare function verbose(enabled: boolean): void;
export declare function log(...args: any[]): void;
