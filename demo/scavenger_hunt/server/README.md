### Federated server

Collects the updates from the various clients/ workers connected to it

run :
 
 	yarn   // installs all the packages and modules required
 
 	yarn postinstall  //runs postinstall script
 	
 	yarn dev //hosts a local federated server
 	
check /dist for models and data(optional) from the clients