### Clients (/workers) 

 Clients are devices with browsers that run the models either on the private data or on the data distruibuted by the model's HOST
 
 How get the client up and running-
 (Run after setting up the federated server)
 
 run :
 
 	yarn   // installs all the packages and modules required
 
 	yarn postinstall  // runs postinstall script
 	
 	yarn demo 	//should start the localhost server client @ port1234
 	
 	
Once the client downloads the model (check index.js), the weights are extracted from the package and made trainable

A random object (label) from the available list of classes for the model is selected

The client is requested to the upload an image containing the object

The model is trained on the pointed image and the updated model is sent to the federated server